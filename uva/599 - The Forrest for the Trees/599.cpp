#include <bits/stdc++.h>
using namespace std;

typedef vector<int> vi;
typedef vector<vi> vvi;

int let2num(char let) {
    return let - 'A';
}

void dfs(int i, vector<bool>&occupied, vvi&adjList) {
    stack<int> stck;
    stck.push(i);
    while (!stck.empty()) {
        int node = stck.top(); stck.pop();
        occupied[node] = true;
        for (int child : adjList[node]) {
            if (!occupied[child]) {
                stck.push(child);
            }
        }
    }
}

void testCase() {
    vvi adjList(27, vi());
    string s;
    while (true) {
        getline(cin, s);
        if (s[0] == '*') break; 
        int a = let2num(s[1]);
        int b = let2num(s[3]);
        adjList[a].push_back(b);
        adjList[b].push_back(a);
    }
    getline(cin, s);
    if (s == "") {
        cout << "There are 0 tree(s) ans 0 acorn(s)." << endl;
        return;
    }
    vi V;
    istringstream is(s);
    char c; is>>c;
    V.push_back(let2num(c));
    while (is) {
        is >> c >> c;
        V.push_back(let2num(c));
    }

    vector<bool> occupied(27, false);
    int trees = 0; int acorns = 0;
    for (int i : V) {
        if (!occupied[i]) {
            if (adjList[i].empty()) {
                occupied[i] = true;
                acorns++;
            }
            else {
                dfs(i, occupied, adjList);
                trees++;
            }
        }
    }
    
    cout << "There are " << trees << " tree(s) and "
         << acorns << " acorn(s)." << endl;
}


int main() {
    int t; cin >> t;
    string junk;
    getline(cin, junk);
    while (t--) {
        testCase();
    }

    return 0;
}