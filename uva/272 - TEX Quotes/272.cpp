#include <bits/stdc++.h>

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    vector<string> lines;
    string line;
    while (getline(cin, line)) {
        lines.push_back(line);
    }

    string output;
    bool first = true;
    for (auto line : lines) {
        for (char c : line) {
            if (c == '"') {
                output += first ? "``" : "''";
                first = !first;
            } else {
                output += c; 
            }
        }
        output += "\n";
    }
    cout << output.substr(0, output.length()-1) << endl;
    
    return 0;
}
