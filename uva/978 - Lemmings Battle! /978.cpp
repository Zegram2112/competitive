#include <bits/stdc++.h>
using namespace std;

struct Lemming {
    int i; int val;

    Lemming fight(const Lemming other) const {
        return {i, val - other.val};
    }
};

bool operator<(const Lemming &l, const Lemming &r) {
    if (l.val == r.val) return l.i < r.i;
    return l.val > r.val;
}

void testCase() {
    int b, sg, sb; cin >> b >> sg >> sb;
    set<Lemming> sgreen, sblue;
    int val;
    for (int i = 0; i < sg; ++i) {
        cin >> val;
        sgreen.insert({i, val});
    }
    for (int i = 0; i < sb; ++i) {
        cin >> val;
        sblue.insert({i, val});
    }
    
    queue<Lemming> battles;
    //cout << "Starting sizes: " << sgreen.size()<<" "<<sblue.size()<<endl;
    while ((!sgreen.empty()) && (!sblue.empty())) {
        for (int i = 0; i < b; ++i) {
            if (sgreen.empty() || sblue.empty()) {
                break;
            }
            auto git = sgreen.begin();
            auto g = *git;
            sgreen.erase(git);
            auto bit = sblue.begin();
            auto b = *bit;
            sblue.erase(bit);
            //cout << g.val << " and " << b.val << " fights" << endl;
            battles.push(g.fight(b));
            battles.push(b.fight(g));
        }
        while (!battles.empty()) {
            Lemming g = battles.front(); battles.pop();
            Lemming b = battles.front(); battles.pop();
            //cout << "Fight results: " << g.val << " " << b.val << endl;
            if (g.val > 0) {
                sgreen.insert(g);
            }
            if (b.val > 0) {
                sblue.insert(b);
            }
        }
        //cout << "Round results: " << sgreen.size() << " " << sblue.size() <<endl;
    }
    if (sgreen.empty() and sblue.empty()) {
        cout << "green and blue died" << endl;
    } else if (sblue.empty()) {
        cout << "green wins" << endl;
        for (Lemming l : sgreen) {
            cout << l.val << endl;
        }
    } else {
        cout << "blue wins" << endl;
        for (Lemming l : sblue) {
            cout << l.val << endl;
        }
    }
}

int main() {
    int t; cin >> t;
    while (t--) {
        testCase();
        if (t != 0) cout << endl;
    }
    return 0;
}
