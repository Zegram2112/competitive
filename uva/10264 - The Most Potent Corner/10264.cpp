#include <bits/stdc++.h>
using namespace std;


int potency(int c, const vector<int> & w) {
    int n = log2(w.size());
    int count = 0;
    for (int i = 0; i < n; ++i) {
        int nebr = c ^ (1 << i);
        count += w[nebr]; 
    }
    return count;
}


int testCase() {
    int n;
    cin >> n;
    if (!cin) {
        return -1;
    }
    vector<int> weights(pow(2,n), 0);
    for (int i = 0; i < pow(2, n); ++i) {
        cin >> weights[i];
    }

    int m = 0;
    for (int c = 0; c < pow(2, n) - 1; ++c) {
        for (int i = 0; i < n; ++i) {
            int nebr = c ^ (1 << i);
            m = max(potency(c, weights) + potency(nebr, weights), m);
        }
    }

    return m;
}


int main() {

    int m;
    while (true) {
        m = testCase();
        if (m == -1) break;
        cout << m << endl;
    }

}
