#include <bits/stdc++.h>
using namespace std;

typedef vector<string> vs;
typedef vector<char> vc;


class HandScorer {
public:

    pair<int, bool> handScore(const vs & hand) {
        initHandInfo();
        int score = 0;
        int trumpScore;
        for (const string & card : hand) {
            suitQuantity[card[1]] += 1;
        };
        for (const string & card : hand) {
            char suit = card[1];
            int sameSuit = suitQuantity[suit];
            switch (card[0]) {
                case 'A':
                    score += 4;
                    stopped[suit] = true;
                    break;
                case 'K':
                    score += 3;
                    if (sameSuit == 1) score -=1;
                    stopped[suit] = stopped[suit] || (sameSuit >= 2);
                    break;
                case 'Q':
                    score += 2;
                    if (sameSuit == 1 || sameSuit == 2)
                        score -= 1;
                    stopped[suit] = stopped[suit] || (sameSuit >= 3);
                    break;
                case 'J':
                    score += 1;
                    if (sameSuit == 3 || sameSuit == 1 || sameSuit == 2)
                        score -= 1;
                    break;
            } 
        }
        bool noTrump = score >= 16;
        for (char suit : suits) {
            switch (suitQuantity[suit]) {
                case 2:
                    score += 1;
                    break;
                case 1:
                    score += 2;
                    break;
                case 0:
                    score += 2;
                    break;
            }
            noTrump = noTrump && stopped[suit];
        }
        return pair<int, bool>{score, noTrump};
    }

    char largestSuit() {
        char max;
        int maxQuantity = 0;
        for (char suit : suits) {
            if (suitQuantity[suit] > maxQuantity) {
                max = suit;
                maxQuantity = suitQuantity[suit];
            }
        }
        return max;
    }

private:

    void initHandInfo() {
        for (char c : suits) {
            suitQuantity[c] = 0;
            stopped[c] = false;
        }
    }

    const vc suits = {
        'S', 'H', 'D', 'C'
    };
    unordered_map<char, int> suitQuantity;
    unordered_map<char, bool> stopped;
};

int main() {
    iostream::sync_with_stdio(false);
    cin.tie(NULL);

    HandScorer scorer;
    string line, card;
    vs cards;
    while (getline(cin, line)) {
        cards.clear();
        stringstream ss(line);
        while (ss >> card) {
            cards.push_back(card);
        }
        pair<int, bool> score = scorer.handScore(cards);
        if (score.first < 14) {
            cout << "PASS" << endl;
        } else {
            cout << "BID ";
            if (score.second) {
                cout << "NO-TRUMP";
            } else {
                cout << scorer.largestSuit();
            }
            cout << endl;
        }
    }

    return 0;
}