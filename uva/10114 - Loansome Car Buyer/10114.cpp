#include <bits/stdc++.h>
using namespace std;

struct Info {
    int months;
    float down, loan;
    unordered_map<int, float> records;
};


int simulate(const Info & info) {
    float carValue = info.down + info.loan;
    carValue -= carValue * info.records.at(0);
    float curLoan = info.loan;
    float payment = info.loan / info.months;

    int curMonth = 0;
    float curDeprecation = info.records.at(0);
    while (curLoan >= carValue) {
        ++curMonth;
        if (info.records.find(curMonth) != info.records.cend()) {
            curDeprecation = info.records.at(curMonth);
        }
        curLoan -= payment;
        carValue -= carValue * curDeprecation;
    }
    return curMonth;
}

int main() {
    iostream::sync_with_stdio(false);
    cin.tie(NULL);

    Info info;
    int k, a;
    float b;
    while (true) {
        info = Info();
        cin >> info.months >> info.down 
            >> info.loan >> k;
        if (info.months < 0) {
            break;
        }
        for (int i = 0; i < k; ++i) {
            cin >> a >> b;
            info.records[a] = b;
        }
        int result = simulate(info);
        cout << result <<
            (string(" month") + (result == 1 ? "":"s")) << endl;
    }
    
    return 0;
}
