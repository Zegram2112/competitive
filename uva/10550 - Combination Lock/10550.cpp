#include <bits/stdc++.h>
using namespace std;

int clockwise(int from, int to) {
    if (from < to) {
        from += 40;
    }
    return (from - to) * 9;
}

int counterwise(int from, int to) {
    return 360 - clockwise(from, to);
}

int main() {
    iostream::sync_with_stdio(false);
    cin.tie(NULL);

    int num; vector<int> nums(4, 0);
    while (cin) {
        bool end = true;
        for (int i = 0; i < 4; ++i) {
            cin >> num;
            nums[i] = num;
            if (num != 0) end = false;
        }
        if (end) {
            break;
        }
        cout << (1080 + clockwise(nums[0], nums[1]) +
            counterwise(nums[1], nums[2]) +
            clockwise(nums[2], nums[3]))
            << "\n";
    }


    return 0;
}
