#include <bits/stdc++.h>
using namespace std;

int main() {
    iostream::sync_with_stdio(false);
    cin.tie(NULL);

    int t, n;
    cin >> t;
    float vel, maxVel;
    for (int i = 0; i < t; ++i) {
        cin >> n;
        maxVel = -1;
        for (int j = 0; j < n; ++j) {
            cin >> vel;
            maxVel = max(vel, maxVel);
        }
        cout << "Case " << i + 1 << ": " << maxVel << endl;
    }
}