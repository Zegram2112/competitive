#include <bits/stdc++.h>
using namespace std;

void testCase(){
	map<string, int> trees;
	float tot = 0;
	string tree;
	while (true){
		getline(cin, tree);
		if (tree == "") {
			break;
		}
		tot++;
		if (trees.find(tree) == trees.end()) {
			trees[tree] = 1;
		} else {
			trees[tree]++;
		}
	}
	for (auto p : trees) {
		cout << p.first + " "<< fixed << setprecision(4) << ((float)p.second)/tot * 100<< "\n";
	}
}

int main(){
	

	int n; cin >> n;
	string s;
	getline(cin, s);
	getline(cin, s);
	for (int i = 0; i< n;++i){
        if (i != 0) cout << endl;
		testCase();
	}
  
	return 0;
}