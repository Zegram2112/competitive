#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

struct RatPop {
    int x, y, size;
};

ll killed[1025][1025];

int main() {
    int t; cin >> t;
    int n, d;
    while (t--) {
        cin >> d >> n;
        for (int i = 0; i < 1025; ++i) {
            for (int j = 0; j < 1025; ++j) {
                killed[i][j] = 0;
            }
        }
        vector<RatPop> rats(n);
        for (int i = 0; i < n; ++i) {
            RatPop & rat = rats[i];
            cin >> rat.x >> rat.y >> rat.size;
        }
        for (auto rat : rats) {
            for (int x = max(rat.x-d, 0); x <= min(rat.x+d, 1024); ++x) {
                for (int y = max(rat.y-d, 0); y <= min(rat.y+d, 1024); ++y) {
                    killed[x][y] += rat.size;
                }
            }
        }
        ll xmax=0, ymax=0, smax=0;
        for (int x = 0; x < 1024; ++x) {
            for (int y = 0 ; y < 1024; ++y) {
                if (killed[x][y] > smax) {
                    xmax = x;
                    ymax = y;
                    smax = killed[x][y];
                }
            }
        }
        cout << xmax << " " << ymax << " " << smax << endl;
    } 
    return 0;
}