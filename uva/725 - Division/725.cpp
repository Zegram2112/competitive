#include <bits/stdc++.h>

using namespace std;

void checkDigits(int num, bitset<10> &digs) {
    while (num != 0) {
        int dig = num % 10;
        num /= 10;
        digs[dig] = 1;
    }
}

int main() {
    int n;
    cin >> n;
    while (true) {
        bool exists = false;
        for (int num = 1234; num <= 98765 / n; ++num) {
            int num2 = num * n;
            bitset<10> digits;
            digits[0] = (num < 10000 || num2 < 10000);
            checkDigits(num, digits);
            checkDigits(num2, digits);
            if (digits.all()) {
                exists = true;
                printf("%0.5d / %0.5d = %d\n", num2, num, n);
            }
        }
        if (!exists)
            printf("There are no solutions for %d.\n", n);
        cin >> n;
        if (n == 0) break;
        printf("\n");
    }
    return 0;
}