#include <bits/stdc++.h>
using namespace std;

struct Cons {
    int a, b, c;
};

int main() {
    int n, m;
    cin >> n >> m;
    while (true) {
        vector<Cons> cs;
        for (int i = 0; i < m; ++i) {
            Cons c;
            cin >> c.a >> c.b >> c.c;
            cs.push_back(c);
        }
        vector<int> v;
        for (int i = 0; i < n; ++i) {
            v.push_back(i);
        }

        int count = 0;
        do {
            bool can = true;
            for (int i = 0; i < m; ++i) {
                Cons cons = cs[i];
                bool f;
                int pos_a, pos_b;
                for (int j = 0; j < n; ++j) {
                    if (v[j] == cons.a) pos_a = j;
                    if (v[j] == cons.b) pos_b = j;
                }
                if (cons.c >= 0) {
                    f = abs(pos_a - pos_b) <= cons.c;
                } else {
                    f = abs(pos_a - pos_b) >= -cons.c;
                }
                if (!f) {
                    can = false;
                    break;
                }
            }
            if (can) ++count;
        } while (next_permutation(v.begin(), v.end()));

        cout << count << endl;
        cin >> n >> m;
        if (n == 0) break;
    }

    return 0;
}
