#include <bits/stdc++.h>
using namespace std;

typedef vector<int> vi;
typedef vector<vi> vvi;

struct Board {
    vi rows;
    int a, b;
    int n;
    Board(int a, int b): a(a), b(b) {
        n = 1;
    }

    bool possible(int x, int y) {
        if (collides(x, y, a, b)) {
            return false;
        }
        for (int i = 0; i < rows.size(); ++i) {
            if (collides(x, y, rows[i], i)) return false;
        }
        return true;
    }

    bool collides(int x, int y, int i, int j) {
        return ((abs(x-i) == abs(y-j)) || y == j || x == i);
    }

    void backtrack(int col=0) {
        if (col == 8) {
            print();
        } else if (col == b) {
            rows.push_back(a);
            backtrack(col+1);
            rows.pop_back();
        } else {
            for (int row = 0; row < 8; ++row) {
                if (possible(row, col)) {
                    rows.push_back(row);
                    backtrack(col+1);
                    rows.pop_back();
                }
            }
        }
    }

    void print() {
        cout << (n < 10?" ":"") << n << "      ";
        for (int i = 0; i < rows.size(); ++i) {
            cout << rows[i] + 1 << " \n"[i == rows.size() - 1];
        }
        ++n;
    }

};

int main()  {

    int t; cin >> t;
    int a, b;
    while (t--) {
        cin >> a >> b;
        Board board(a-1, b-1);
        cout << "SOLN       COLUMN" << endl
            << " #      1 2 3 4 5 6 7 8" << endl << endl;

        board.backtrack();
        if (t != 0) cout << endl;
    }

    return 0;
}