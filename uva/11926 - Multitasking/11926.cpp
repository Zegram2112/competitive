#include <bits/stdc++.h>
using namespace std;

struct Calendar {

    bitset<1000001> cal;

    bool addOne(int a, int b) {
        for (int i = a + 1; i <= b; ++i) {
            if (cal.test(i)) {
                return false;
            }
            cal.set(i);
        }
        return true;
    }

    bool addRepeating(int a, int b, int reps) {
        while (a < 1000000) {
            bool res = addOne(a, b);
            if (!res) return false;
            a += reps;
            b = min(b + reps, 1000000);
        }
        return true;
    }

};


int main() {
    cin.tie(NULL);
    iostream::sync_with_stdio(false);
    int n, m;
    int a, b, reps;
    vector<int> A, B, R;
    bool conflict;
    Calendar cal;
    while (true) {
        conflict = false;
        cal.cal.reset();
        cin >> n >> m;
        if (n == 0 && m == 0) {
            break;
        }
        for (int i = 0; i < n; ++i) {
            cin >> a >> b;
            A.push_back(a);
            B.push_back(b);
            R.push_back(0);
        }
        for (int i = 0; i < m; ++i) {
            cin >> a >> b >> reps;
            A.push_back(a);
            B.push_back(b);
            R.push_back(reps);
        }
        for (size_t i = 0; i < A.size(); ++i) {
            if (R[i] == 0) {
                conflict = conflict || !cal.addOne(A[i], B[i]);
            } else {
                conflict = conflict || !cal.addRepeating(A[i], B[i], R[i]);
            }
            if (conflict) break;
        }
        cout << (conflict?"":"NO ") << "CONFLICT" << endl;
        A.clear();
        B.clear();
        R.clear();
    }
    return 0;
}
