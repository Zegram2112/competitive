#include <bits/stdc++.h>
using namespace std;

struct Contestant {

    int id;
    bool _solved[10] = {false};
    int _penalty[10] = {0};

    int solved() const {
        int count = 0;
        for (bool b : _solved) {
            if (b) ++count; 
        }
        return count;
    }

    int penalty() const {
        int count = 0;
        for (int i = 0; i < 10; ++i) {
            int p = _penalty[i];
            if (_solved[i]) {
                count += p; 
            }
        }
        return count;
    }

    void addProblem(int id, int time, char l) {
        if (_solved[id]) {
            return;  
        }
        if (l == 'C') {
            _solved[id] = true;
            _penalty[id] += time;
        } else if (l == 'I') {
            _penalty[id] += 20;
        }
    }

};


bool lesser(const Contestant & c1, const Contestant & c2) {
    if (c1.solved() != c2.solved()) {
        return (c1.solved() < c2.solved());
    } else if (c1.penalty() != c2.penalty()) {
        return (c1.penalty() > c2.penalty());
    }
    return (c1.id > c2.id);
}


struct Input {
    int id, problem, time;
    char l;
};


void testCase() {
    string line;
    map<int, Contestant> conts;
    vector<Input> inputs;
    while (true) {
        getline(cin, line);
        if (line == "") break;
        istringstream iss(line);
        Input input;
        iss >> input.id >> input.problem >> input.time >> input.l;
        inputs.push_back(input);
    } 
    
    sort(inputs.begin(), inputs.end(), [](Input & i1, Input & i2){return i1.time < i2.time;});


    for (Input input : inputs) {
        map<int, Contestant>::iterator it = conts.find(input.id);
        if (it == conts.end()) {
            conts[input.id].id = input.id;
        }
        conts[input.id].addProblem(input.problem, input.time, input.l);
    }

    
    vector<Contestant> v;

    for (auto p : conts) {
        v.push_back(p.second);
    }

    sort(v.begin(), v.end(), lesser);
    for(auto it = v.rbegin(); it != v.rend(); ++it) {
        auto c = *it;
        cout << c.id << " " << c.solved() << " " << c.penalty() << endl;
    }
}


int main() {

    int n;
    string junk;
    cin >> n;
    getline(cin, junk);
    getline(cin, junk);
    for (int i = 0 ; i < n; ++i) {
        testCase();
        if (i != n-1) {
            cout << endl;
        }
    }

    return 0;
}




