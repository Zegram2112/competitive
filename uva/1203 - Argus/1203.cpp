#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

struct Query {
    int qnum;
    int period;
    int cperiod;
};

struct Comparator {
    bool operator()(const Query & l, const Query & r) const {
        if (l.period == r.period) {
            return l.qnum > r.qnum;
        }
        return l.period > r.period;
    }
};

int main() {

    string s;
    Query q;
    priority_queue<Query,
        vector<Query>, Comparator> pq;
    while (true) {
        cin >> s;
        if (s != "Register") break;
        cin >> q.qnum >> q.period;
        q.cperiod = q.period;
        pq.push(q);
    }
    int n; cin >> n;
    for (int i = 0; i < n; ++i) {
        Query q = pq.top(); pq.pop();
        cout << q.qnum << endl;
        q.period += q.cperiod;
        pq.push(q);
    }

    return 0;
}