#include <bits/stdc++.h>
using namespace std;

void testCase() {
    int n; cin >> n;
    map<int, int> m;
    int u = -1;
    int mx = 0;
    int x;
    for (int i = 0; i < n; ++i) {
        cin >> x;
        if (m.find(x) != m.end()) {
            if (m[x] > u) {
                mx = max(mx, i-(u+1));
                u = m[x];
            }
        }
        m[x] = i;
    }
    mx = max(mx, n-(u+1));

    cout << mx << endl;
}

int main() {

    int t; cin >> t;
    while (t--) {
       testCase(); 
    }

    return 0;
}
