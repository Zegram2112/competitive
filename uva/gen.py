import os
import sys
import requests

def main():
    if len(sys.argv) == 2:
        url = "https://uhunt.onlinejudge.org/api/p/num/" + str(sys.argv[1])
        data = requests.get(url).json()
        folder = f"{data['num']} - {data['title']}"
        os.system(f'mkdir "{folder}"')
        os.system(f'cd "{folder}"')
        open(f"{folder}/{data['num']}.cpp", "w").close()

main()
