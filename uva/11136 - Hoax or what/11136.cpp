#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

bool testCase() {
    int n; cin >> n;

    if (n == 0) return false;
    multiset<int> urn;

    int k, bill;
    ll s = 0;
    for(int d = 0; d<n; ++d) {
        cin >> k;
        for (int i = 0; i < k; ++i) {
            cin >> bill;
            urn.insert(bill);
        }
        
        if (urn.size() >= 2) {
            int mx = *(urn.rbegin());
            int mn = *(urn.begin());
            s += mx - mn;
            urn.erase(urn.begin());
            if (!urn.empty())urn.erase(prev(urn.end()));
        }
    }

    cout << s << endl;
    return true;
}

int main() {

    while (true) {
        bool ans = testCase();
        if (!ans) break;
    }

    return 0;
}
