#include <bits/stdc++.h>
using namespace std;

typedef pair<int, int> ii;
typedef vector<ii> vii;
typedef vector<vii> vvii;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<bool> vb;

void testCase(int m, int n) {
    vvii adjList(m+1, vii());
    vvii outList(n+1, vii());
    int val;
    // construir adjList
    for (int i = 1; i <= m; ++i) {
        int ncols;
        cin >> ncols;
        vi cols(ncols, 0);
        for (int j=0; j < ncols; ++j) {
            cin >> val;
            cols[j] = val;
        }
        for (int col : cols) {
            cin >> val;
            adjList[i].push_back({col, val});
        }
    }
    // construir la traspuesta
    for (int i = 1; i <= m; ++i) {
        for (ii p : adjList[i]) {
            outList[p.first].push_back({i, p.second});
        }
    }

    cout << n << " " << m << endl;
    for (int i = 1; i <= n; ++i) {
        cout << outList[i].size();
        for (ii p : outList[i]) {
            cout << " " << p.first;
        }
        cout << endl;
        for (int j = 0; j < outList[i].size(); ++j) {
            if (j != 0) cout << " ";
            cout << outList[i][j].second;
        }
        cout << endl;
    }
    return;
}

int main() {

    int m, n;
    while (cin >> m) {
        cin >> n;
        testCase(m, n);
    }
    return 0;
}