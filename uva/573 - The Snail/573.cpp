#include <bits/stdc++.h>
using namespace std;

struct Info {
    int H, U, D;
    float F;
};

int solve(const Info & info) {
    float curHeight = 0.0f;
    int day = 0;
    while (true) {
        curHeight += max(0.0f, info.U*(1 - (info.F/100)*day));
        if (curHeight > info.H) {
            return (day + 1) ;
        }
        curHeight -= info.D;
        if (curHeight < 0) {
            return (day + 1) * -1;
        }
        ++day;
    }
}

int main() {
    iostream::sync_with_stdio(false);
    cin.tie(NULL);

    Info info;
    while (true) {
        cin >> info.H >> info.U >> info.D >> info.F;
        if (info.H == 0) break;
        int res = solve(info);
        cout << ((res >= 0) ? "success" : "failure") << " on day " << abs(res) << endl;
    }

    return 0;
}