#include <bits/stdc++.h>

using namespace std;

struct v2{
    int x;
    int y;

    v2 operator+ (const v2& other) {
        return v2{x + other.x, y + other.y}; 
    }

    v2 operator* (int n) {
        return v2{x * n, y * n};
    }

};


ostream& operator<< (ostream& out, const v2 & v) {
    out << "{" << v.x << "," << v.y << "}";
    return out;
}


v2 cartCoords(int n, long p) {
    long curPos = 1;
    v2 cartPos = v2{n/2,n/2};

    vector<v2> dirs = {v2{0,1}, v2{-1,0}, v2{0,-1}, v2{1,0}};
    int curDir = 0;
    long step = 1;
    int stepsDone = 0;

    while (curPos < p) {
        //cout << cartPos << " " << curPos << endl;
        if (curPos + step < p) {
            if (step < n-1 && stepsDone >= 2) {
                ++step;
                stepsDone = 0;
            }
            cartPos = cartPos + dirs[curDir] * step;
        } else {
            cartPos = cartPos + dirs[curDir] * (p - curPos);
        }
        curPos += step;
        ++stepsDone;
        curDir = (curDir+1)%4;
    }
    return cartPos;
}

int main() {
    cin.tie(0);
    iostream::sync_with_stdio(false);
    
    int n; long p;
    while (true) {
        cin >> n >> p;
        if (n == 0 && p == 0) {
            break;
        }
        v2 res = cartCoords(n, p);
        cout << "Line = " << res.y + 1 << ", column = " << res.x + 1 << "." <<  endl;
    }

    return 0;
}
