#include <bits/stdc++.h>
using namespace std;

typedef vector<int> vi;
typedef vector<vi> vvi;

struct Ufds {
    map<string, string> p;
    map<string, int> rank, size;
    Ufds() {
    }
    string findSet(string & s1) {
        if (p.find(s1) == p.end()) {
            p[s1] = s1;
            rank[s1] = 0;
            size[s1] = 1;
            return s1;
        }
        else if (p[s1] == s1) {
            return s1;
        } else {
            string root = findSet(p[s1]);
            p[s1] = root;
            return root;
        }
    }
    bool sameSet(string &s1, string&s2) {
        return findSet(s1)==findSet(s2);
    }
    void unionSet(string &s1, string&s2) {
        if (!sameSet(s1, s2)) {
            string i = findSet(s1);
            string j = findSet(s2);
            if (rank[i] > rank[j]) {
                p[j] = i;
                size[i] = size[j] + size[i];
            } else if (rank[j] > rank[i]) {
                p[i] = j;
                size[j] = size[i] + size[j];
            } else {
                p[i] = j;
                rank[j]++;
                size[j] = size[j] + size[i];
            }
        }
    }
    int findSize(string &s) {
        string root = findSet(s);
        return size[root];
    }
};

void testCase() {
    int f;
    cin >> f;
    Ufds network;
    string s1, s2;
    for (int i = 0; i < f; ++i) {
        cin >> s1 >> s2;
        network.unionSet(s1, s2); 
        cout << network.findSize(s1) << endl;
    }
}

int main() {

    int n;
    cin >> n;
    for (int i = 0; i < n; ++i) {
        testCase();
    }

    return 0;
}