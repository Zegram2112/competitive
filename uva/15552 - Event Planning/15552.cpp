#include <bits/stdc++.h>
using namespace std;



int main() {
    iostream::sync_with_stdio(false);
    cin.tie(NULL);

    int n, h, w;
    float budget;
    int price, avail;
    while (cin>>n) {
        cin >> budget >> h >> w;
        int cost = -1;
        for (int i = 0; i < h; ++i) { // for each hotel
            cin >> price;
            for (int j = 0; j < w; ++j) {  // for each weekend
                cin >> avail;
                if (n <= avail && price*n <= budget) {
                    cost = (cost == -1)? price*n : min(cost, price*n);
                }
            }
        } 
        if (cost == -1) { 
            cout << "stay home" << endl;
        } else {
            cout << cost << endl;
        }
    }
    return 0;
}