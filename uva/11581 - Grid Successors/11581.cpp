#include <bits/stdc++.h>
#define vvi vector<vector<int>>
#define vi vector<int>
#define FOR(i, n) for(int i = 0; i < n; ++i)

using namespace std;

bool isZero(const vvi & v) {
    FOR(i, 3) {
        FOR(j, 3) {
            if (v[i][j] == 1) return false;
        }
    }
    return true;
}

int x[4] = {-1, 0, 1, 0};
int y[4] = {0, -1, 0, 1};
int f(vvi v, int l=0) {
    if (isZero(v)) {
        return -1;
    }

    vvi newv(3, vi(3,0));
    FOR(i, 3) {
        FOR(j, 3) {
            FOR(d, 4) {
                int a = i + y[d];
                int b = j + x[d];
                if (a >= 0 && a < 3 && b >= 0 && b < 3) {
                    newv[i][j] += v[a][b];
                }
            }
            newv[i][j] %= 2;
        }
    }

    return 1 + f(newv, l+1);
}

int main() {
    vvi v(3, vi(3, 0));

    int n;
    char c;
    cin >> n;
    FOR(w, n) {
        FOR(i, 3) {
            FOR(j, 3) {
                cin >> c;
                v[i][j] = c - '0';
            }
        }
        cout << f(v) << endl;
    }
    
    return 0;
}
