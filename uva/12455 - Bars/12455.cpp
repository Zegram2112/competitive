#include <bits/stdc++.h>
using namespace std;

int main() {
    int t; cin >> t;
    while (t--) {
        int n, p; cin >> n >> p;
        vector<int> v;
        v.assign(p, 0);
        for (int i = 0; i < p; ++i) {
            cin >> v[i];
        }
        bool can = false;
        for (int bm = 0; bm < (1 << p); ++bm) {
            int sum = 0;
            for (int i = 0; i < p; ++i) {
                if ((1 << i) & bm) {
                    sum += v[i];
                }
            }
            if (sum == n) {
                cout << "YES" << endl;
                can = true;
                break;
            }
        }
        if (!can) cout << "NO" << endl;

    }
    return 0;
}