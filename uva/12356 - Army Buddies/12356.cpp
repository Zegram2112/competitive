#define LOOP(n) for (int i = 0; i < n; ++i) 
#include <bits/stdc++.h>
using namespace std;


bool testCase() {
    int s, repos;
    cin >> s >> repos;
    if (s == 0) {
        return false;
    }

    int rbuds[s];
    int lbuds[s];
    LOOP(s) {
        rbuds[i] = i + 1;
        lbuds[i] = i - 1;
    }
    rbuds[s-1] = -1;

    int l, r;  
    LOOP(repos) {
        cin >> l >> r;
        l = l - 1;
        r = r - 1;

        rbuds[lbuds[l]] = rbuds[r];
        lbuds[rbuds[r]] = lbuds[l];

        if (lbuds[l] == -1) {
            cout << "*";
        } else {
            cout << lbuds[l] + 1;
        }
        cout << " ";

        if (rbuds[r] == -1) {
            cout << "*";
        } else {
            cout << rbuds[r] + 1;
        }
        cout << endl;

    }

    cout << "-" << endl;

    return true;
}

int main() {
    iostream::sync_with_stdio(false);
    cin.tie(NULL);
    
    while (testCase()) {
    }

    return 0;
}