#include <bits/stdc++.h>
using namespace std;

typedef set<int> courses;

int testCase() {
    int n;
    map<courses, int> popularity;
    map<int, courses> stCourse;
    cin >> n;
    if (n == 0) return -1;

    int val;
    for (int i = 0; i < n; ++i) {
        courses c;
        for (int j = 0 ; j < 5; ++j) {
            cin >> val;
            c.insert(val);
        }
        stCourse[i] = c;
        if (popularity.find(c) == popularity.end()) {
            popularity[c] = 1;
        } else {
            popularity[c]++;
        }
    }

    auto p = max_element(popularity.begin(), popularity.end(),
        [](const pair<courses, int> &l,const  pair<courses, int> &r) { return l.second < r.second;});
    int maxPop = (*p).second;
    set<courses> maxCourses;
    for (auto & p : popularity) {
        if (p.second == maxPop) {
            maxCourses.insert(p.first);
        }
    }

    int students = 0;
    for (int i = 0; i < n; ++i) {
        if (maxCourses.find(stCourse[i]) != maxCourses.end()) {
            students++;
        }
    }

    cout << students << endl;

    return 0;
}

int main() {

    while (testCase() != -1) {
    }

    return 0;
}