#include <bits/stdc++.h>
using namespace std;

typedef vector<int> vi;

struct Ufds {
    vi p, rnk;
    Ufds(int n) {
        p.assign(n+1, 0);
        rnk.assign(n+1, 0);
        for (int i = 0; i < n; ++i) {
            p[i] = i;
        }
    }
    int findSet(int i) {
        if (p[i] == i) return i;
        else {
            int ans = findSet(p[i]);
            p[i] = ans;
            return ans;
        }
    }
    void unionSet(int i, int j) {
        int x = findSet(i);
        int y = findSet(j);
        if (x != y) {
            if (rnk[x] > rnk[y]) {
                p[y] = x; 
            } else {
                p[x] = y;
                if (rnk[x] == rnk[y]) {
                    rnk[y]++;
                }
            }
        }
    }
    bool sameSet(int i, int j) {
        return findSet(i) == findSet(j);
    }
};

void testCase() {
    string line;
    int n; cin >> n;
    getline(cin, line);
    Ufds ufds(n);
    string a; int ci, cj;
    int suc = 0;
    int unsuc = 0;
    while (true){
        getline(cin, line);
        if (!cin || line == "") break;
        istringstream is(line);
        is >> a >> ci >> cj;
        if (a == "c") {
            ufds.unionSet(ci, cj);
        } else if (a == "q") {
            if (ufds.sameSet(ci, cj)) {
                suc++;
            } else unsuc++;
        }
    }
    cout << suc << "," << unsuc << "\n";
}

int main() {
    int t; cin >> t;
    string junk;
    for (int i = 0; i < t; ++i) {
        testCase();
        if (i != t-1) {
            cout << endl;
        }
    }
    return 0 ;
}
