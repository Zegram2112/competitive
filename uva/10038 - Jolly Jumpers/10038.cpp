#include <bits/stdc++.h>
using namespace std;

bool isJolly(vector<int> &nums) {
    int n = nums[0];
    bool flags[n] = {false};
    if (n == 1) {
        return true;
    }
    for (int i = 1; i < nums.size() - 1; ++i) {
        int diff = abs(nums[i+1] - nums[i]);
        //cout << diff << endl;
        if (diff < 1 || diff >= n) {
            return false;
        }
        flags[diff] = true;
    }
    for (int i = 1; i <= n-1; ++i) {
        if (!flags[i]) {
            return false;
        }
    }
    return true;
}

int main() {
    
    string line;
    vector<int> nums;
    int n;
    while(getline(cin, line)) {
        nums.clear();
        stringstream ss(line);
        while (ss >> n) {
            nums.push_back(n);
        }
        cout << ((isJolly(nums)) ? "Jolly" : "Not jolly") << endl;
    }
    
    return 0;
}
