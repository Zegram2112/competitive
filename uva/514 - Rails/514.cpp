#include <bits/stdc++.h>
using namespace std;

bool possible(int n, vector<int> & comb) {

    stack<int> s;
    
    auto next = comb.begin();
    int i = 1;
    while (true) {
        if ((!s.empty()) && *next == s.top()) {
            s.pop(); 
            ++next;
        } else {
            if (i == n + 1) break;
            s.push(i);
            ++i;
        }
    }

    return (next == comb.end());
}

int main() {

    int n, v;
    vector<int> comb;
    while (true) {
        cin >> n;
        if (n == 0) break;
        while (true) {
            comb.clear();
            for (int i = 0; i < n; ++i) {
                cin >> v;
                if (v == 0) {
                    break;
                }
                comb.push_back(v);
            }
            if (v == 0) break;
            cout << (possible(n, comb)?"Yes":"No") << endl;
        }
        cout << endl;
    }

    return 0;
}
