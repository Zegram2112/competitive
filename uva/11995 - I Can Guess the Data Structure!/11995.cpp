#include <bits/stdc++.h>
using namespace std;

void testCase(int n) {
    stack<int> s;
    queue<int> q;
    priority_queue<int> pq;
    bool check[3] = {true, true, true};
    int command, val;
    bool impossible = false;
    for (int i = 0; i<n; ++i) {
        cin >> command >> val;
        if (impossible) continue;
        switch (command) {
            case 1:
                s.push(val);
                q.push(val);
                pq.push(val);
                break;
            case 2:
                if (s.empty()) {
                    impossible = true;
                }
                if (!impossible) {
                    if (check[0]) {
                        check[0] = s.top() == val; s.pop();
                    }
                    if (check[1]) {
                        check[1] = q.front() == val; q.pop();
                    }
                    if (check[2]) {
                        check[2] = pq.top() ==  val; pq.pop();
                    }
                }
                break;
        }
    }
    if (impossible) {
        cout << "impossible" << endl;
        return;
    }
    int count = 0;
    for (bool b : check) count += b ? 1 : 0;
    if (count == 1) {
        for (int i = 0; i < 3; ++i) {
            if (check[i]) {
                switch (i) {
                    case 0:
                        cout << "stack" << endl;
                        break;
                    case 1:
                        cout << "queue" << endl;
                        break;
                    case 2:
                        cout << "priority queue" << endl;
                        break;
                }
            }
        }
    } else if (count == 0) { 
        cout << "impossible" << endl;
    } else {
        cout << "not sure" << endl;
    }
    return;
}

int main() {

    int n;
    while (cin >> n) {
        testCase(n);
    }

}