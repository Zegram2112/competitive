#include <bits/stdc++.h>
using namespace std;


int main() {
    iostream::sync_with_stdio(false);
    cin.tie(NULL);

    string line;
    while (getline(cin, line)) {
        cout << line << "\n";
    }

    return 0;
}
