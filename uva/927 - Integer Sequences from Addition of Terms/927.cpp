#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;

struct Pol {
    ll eval(ll n) {
        ll val = 0;
        ll m = 1;
        for (int i = 0; i < factors.size(); ++i) {
            val += factors[i] * m;
            m *= n;
        }
        return val;
    }

    vi factors;
};

int main() {

    int t; cin >> t;
    int f;
    int d, k;
    int n;
    while (t--) {
        Pol pol;
        cin >> n;
        for (int i = 0; i < n+1; ++i) {
            cin >> f;
            pol.factors.push_back(f);
        }
        cin >> d >> k;


        int i = 1;
        int m = 1;
        int val;
        while (true) {
            if (k >= i && k <= i + m*d - 1) {
                cout << pol.eval(m) << endl;
                break;
            } else {
                i += m*d;
                m += 1;
            }
        }
    }

    return 0;
}