#include <bits/stdc++.h>
using namespace std;

typedef vector<int> vi;
typedef vector<vi> vvi;

map<char, int> char2int;

struct Ufds {
    int n;
    vi rank, p;
    Ufds(int n): n(n) {
        rank.assign(n, 0);
        for (int i = 0; i < n; ++i) {
            p.push_back(i);
        }
    }

    int findSet(int i) {
        if (p[i] == i) {
            return i;
        } else {
            int root = findSet(p[i]);
            p[i] = root;
            return root; 
        }
    } 

    void join(int x, int y) {
        int i = findSet(x);
        int j = findSet(y);
        if (rank[i] > rank[j]) {
            p[j] = i;
        } else if (rank[j] > rank[i]) {
            p[i] = j;
        } else {
            p[i] = j;
            rank[j] += 1;
        }
    }

    bool sameSet(int i, int j) {
        return findSet(i) == findSet(j);
    }
};

int testCase() {
    char2int.clear();
    int n, m;
    cin >> n;
    if (!cin) return -1;
    cin >> m;
    char c;
    Ufds ufds(n);
    int awaken = 0;
    cin >> c;
    char2int[c] = awaken;
    for (int i = 1; i <= 2; ++i) {
        cin >> c;
        char2int[c] = i;
        ufds.join(awaken, i);
    }
    vvi adjList;
    adjList.assign(n, vi());
    char c1, c2;
    int pos = 3;
    for (int i = 0; i < m; ++i) {
        cin >> c1 >> c2;
        if (char2int.find(c1) == char2int.end()) {
            char2int[c1] = pos;
            pos++;
        }
        if (char2int.find(c2) == char2int.end()) {
            char2int[c2] = pos;
            pos++;
        }
        int n1 = char2int[c1];
        int n2 = char2int[c2];
        adjList[n1].push_back(n2);
        adjList[n2].push_back(n1);
    }


    // simulation
    int year = 0;
    int awakenNumber = 3;
    vi to_activate;
    while (true) {
        if (awakenNumber == n) {
            cout << "WAKE UP IN, " << year << ", YEARS" << endl;
            break;
        }

        to_activate.clear();
        int activations = 0;
        /*for (int i : ufds.p) {
            cout << " " << i;
        }
        cout << endl;*/
        for (int i = 0; i < n; ++i) {
            if (!ufds.sameSet(awaken, i)) {
                int adjAwaken = 0;
                for (int conn : adjList[i]) {
                    if (ufds.sameSet(awaken, conn)) adjAwaken++;
                }
                if (adjAwaken >= 3) {
                    to_activate.push_back(i);
                    activations++;
                }
            }
        }
        for (int i : to_activate) {
            ufds.join(awaken, i);
        }
        awakenNumber += activations;
        year++;

        if (activations == 0) {
            cout << "THIS BRAIN NEVER WAKES UP" << endl;
            break;
        }
    }



    return 0;
}

int main() {

    while (true) {
        if (testCase() != 0) break;
    }
    return 0;
}