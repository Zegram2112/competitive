#include <bits/stdc++.h>
using namespace std;

typedef vector<int> vi;

struct SegTree {
    vi st;
    int n;
    vi & a;
    SegTree(int n, vi & a): n(n), a(a) {
        st.assign(4*n + 5, 0);
        build();
    }
    void build() {build(1, 0, n-1);};
    void build(int p, int l, int r) {
        if (l == r) {
            st[p] = a[l];
        } else {
            build(2*p, l, (l+r)/2);
            build(2*p + 1, (l+r)/2 + 1, r);
            st[p] = max(st[2*p], st[2*p + 1]);
        }
    }
    int query(int i, int j) {
        return query(i, j, 1, 0, n-1);
    }
    int query(int i, int j, int p, int l, int r) {
        if (i <= l && r <= j) {
            return st[p];
        } else if (j < l || r < i) {
            return -1;
        } else {
            int q1 = query(i, j, 2*p, l, (l+r)/2);
            int q2 = query(i, j, 2*p + 1, (l+r)/2 + 1, r);
            return max(q1, q2);
        }
    }
};

void print(vi & v) {
    for (int i : v) {
        cout << " " << i;
    }
    cout << endl;
}

bool testCase() {
    int n, m;
    cin >> n;
    if (n == 0) return false;
    cin >> m;
    vi a, freq;
    int x, y;
    for (int i = 0; i < n; ++i) {
        cin >> x;
        a.push_back(x);
    }

    map<int, int> beg, end;
    freq.assign(n, 0);
    int counter = 0;
    for (int i = 0; i < n; ++i) {
        ++counter;
        if (i == n-1 || (a[i] != a[i+1]))  {
            end[a[i]] = i;
            freq[i] = counter;
            counter = 0;
        }
    }
    int current = freq[n-1];
    for (int i = n-2; i >=0; --i) {
        if (freq[i] != 0) {
            current = freq[i];
        } else {
            freq[i] = current;
        }
    }
    for (int i = n-1; i >=0; --i) {
        if (i == 0 || (a[i] != a[i-1])) {
            beg[a[i]] = i;
        }
    }

    SegTree tree(n, freq);
    //print(freq);
    //print(tree.st);
    for (int i = 0; i < m; ++i) {
        cin >> x >> y;
        if (a[x-1] == a[y-1]) {
            cout << y - x + 1 << endl;
            continue;
        }
        int endx = end[a[x-1]];
        int begy = beg[a[y-1]];
        int c1 = endx - (x-1) + 1;
        int c2 = (y-1) - begy + 1;
        int c3;
        if (endx + 1 < begy) {
            c3 = tree.query(endx + 1, begy - 1);
        } else c3 = -1;
        //cout << endx << " " << begy << " " << c1 << " " << c2 << " " << c3 << endl;
        cout << max({c1,c2,c3}) << endl; 
    }

    return true;
}

int main() {
    while (testCase());
    return 0;
}
