#include <bits/stdc++.h>
using namespace std;

class Book {
    public:
        Book(string name, string author):
            name(name), author(author) {}
        string name;
        string author;
        bool borrowed = false;
        bool inShelf = true;

        static bool compare(const Book & bk1, const Book & bk2) {
            if (bk1.author == bk2.author) {
                return bk1.name < bk2.name;
            } else return bk1.author < bk2.author;
        }

};


void testCase() {
    vector<Book> books;
    string line;
    string separator = "\" by ";
    while(true) {
        getline(cin, line);
        if (line == "END") {
            break; 
        }
        line = line.substr(1, line.size()-1);
        size_t sep_pos = line.find(separator); 
        string name = line.substr(0, sep_pos);
        string author = line.substr(sep_pos + separator.size(), line.size() - separator.size() - sep_pos);
        books.emplace_back(name, author);
    }
    sort(books.begin(), books.end(), Book::compare);

    string command, name;
    while (true) {
        cin >> command;
        if (command == "BORROW") {
            getline(cin, name);
            name = name.substr(2, name.size() - 3);
            for (int i = 0; i < books.size(); ++i) {
                if (books[i].name == name) {
                    books[i].borrowed = true;
                    books[i].inShelf = false;
                    break;
                }
            }
        } else if (command == "RETURN") {
            getline(cin, name);
            name = name.substr(2, name.size() - 3);
            for (int i = 0; i < books.size(); ++i) {
                if (books[i].name == name) {
                    books[i].borrowed = false;
                    break;
                }
            }
        } else if (command == "SHELVE") {
            Book * lastInShelf = nullptr;
            for (int i = 0; i < books.size(); ++i) {
                if (books[i].inShelf) {
                    lastInShelf = &books[i];
                } else if (!books[i].borrowed) {
                    cout << "Put \"" << books[i].name << "\" ";
                    if (lastInShelf == nullptr) {
                        cout << "first";
                    } else {
                        cout << "after \"" << lastInShelf->name << "\"";
                    }
                    cout << endl;
                    books[i].inShelf = true;
                    lastInShelf = &books[i];
                }
            } 
            cout << "END" << endl;
        } else if (command == "END") {
            break;
        }
    }
}



int main() {
    iostream::sync_with_stdio(false);
    cin.tie(NULL);

    testCase();

    return 0;
}