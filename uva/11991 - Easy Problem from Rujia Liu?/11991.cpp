#include <bits/stdc++.h>
using namespace std;

typedef vector<int> vi;
typedef vector<vi> vvi;

vvi adjList;

int main() {
    iostream::sync_with_stdio(false);
    cin.tie(NULL);

    int n, m, val, k, v;
    while (cin >> n) {
        cin >> m;

      adjList.assign(1e6+1, vi(1,0));
        for (int i = 1; i <= n; ++i) {
            cin >> val;
            adjList[val].push_back(i);
            adjList[val][0]++;
        }

        for (int i = 0; i < m; ++i) {
            cin >> k >> v;
            if (adjList[v][0] >= k) {
                cout << adjList[v][k] << "\n";
            } else {
                cout << 0 << "\n";
            }
        }

    }

    return 0;
}