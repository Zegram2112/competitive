#include <bits/stdc++.h>
using namespace std;

int main() {

    int k, n, m, x, y;
    while (cin) {
        cin >> k >> n >> m;
        if (k == 0) {
            break;
        }
        string output;
        for (int i = 0; i < k; ++i) {
            cin >> x >> y;
            if (x == n || y == m) {
                output = "divisa";
            } else { 
                output = "";
                if (y > m) {
                    output += "N";
                } else output += "S";
                if (x > n) output += "E";
                else output += "O";
            }
            cout << output << endl;
        }
    }

    return 0;
}
