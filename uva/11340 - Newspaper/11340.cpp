#define LOOP(k) for (int i = 0; i < k; ++i)
#include <bits/stdc++.h>
using namespace std;

string dollars(int cents) {
    ostringstream oss;
    oss << fixed << setprecision(2) << cents/100.0 << "$";
    return oss.str();
}

void testCase() {
    int dat[256] = {0};

    int k;
    unsigned char c;
    cin >> k;
    LOOP(k) {
        cin >> c >> dat[(int)c];
    }
    
    int m, valsum = 0;
    string line;
    istringstream iss;
    cin >> m;
    getline(cin, line);
    LOOP(m) {
        getline(cin, line);
        iss.str(line);
        iss.clear();
        while (iss >> c) {
            valsum += dat[(int)c];
        }
    }

    cout << dollars(valsum) << endl;
}

int main() {
    iostream::sync_with_stdio(false);
    cin.tie(NULL);

    int n;
    cin >> n;
    LOOP(n) {
        testCase();
    }

    return 0;
}