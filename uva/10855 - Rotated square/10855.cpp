#include <bits/stdc++.h>
#define vvc vector<vector<char>>
#define FOR(i,n) for(int i = 0; i < n; ++i)


using namespace std;


vector<vector<char>> rotated(const vector<vector<char>> & v) {
    int n = v.size();
    vector<vector<char>> rotated(n, vector<char>(n, 0));
    for (int j = 0; j < n; ++j) {
        for (int i = 0; i < n; ++i) {
            rotated[n-1-j][i] = v[i][j];
        }
    }
    return rotated;
}


vvc readSquare(int n) {
    vvc v(n, vector<char>(n, 0));
    FOR(i,n) {
        FOR(j,n) {
            cin >> v[i][j];
        }
    }
    return v;
}


void printSquare(const vvc & v) {
    int n = v.size();
    FOR(i,n) {
        FOR(j, n) {
            cout << v[i][j];
        }
        cout << endl;
    }
}


int numAppearances(const vvc & big, const vvc & lit) {
    int N = big.size();
    int n = lit.size();
    int aps = 0;
    FOR(i, N-n+1) {
        FOR(j, N-n+1) {
            if (big[i][j] == lit[0][0]) {
                bool ap = true;
                FOR(ii, n) {
                    FOR(jj, n) {
                        if (big[i + ii][j + jj] != lit[ii][jj]) {
                            ap = false;
                            break;
                        }
                    }
                    if (!ap) break;
                }
                if (ap) {
                    ++aps;
                }
            }
        }
    }
    return aps;
}


int main() {
    // cin.tie(NULL);
    // iostream::sync_with_stdio(false);
    
    int n, N;
    while (true) {
        cin >> N >> n;
        if (N == 0) break;
        vvc big = readSquare(N);
        vvc lit = readSquare(n);
        cout << numAppearances(big, lit) << " ";
        FOR(i, 3) {
            big = rotated(big);
//            printSquare(big);
            cout << numAppearances(big, lit) << (i==2?"":" ");
        }
        cout << endl;
    }

    return 0;
}

