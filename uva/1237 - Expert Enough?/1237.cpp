#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

struct Info {
    string name;
    ll l, h; 
};

int main() {

    int t; cin >> t;
    while (t--) {
        int d; cin >> d;
        vector<Info> db;
        Info inf;
        for (int i = 0; i < d; ++i) {
            cin >> inf.name >> inf.l >> inf.h;
            db.push_back(inf);
        }
        int q; cin >> q;
        ll p; 
        for (int i = 0; i < q; ++i) {
            int count = 0;
            int last_j;
            cin >> p;
            for (int j = 0; j < d; ++j) {
                if (db[j].l <= p && p <= db[j].h) {
                    count++;
                    last_j = j;
                }
            }
            if (count == 1) cout << db[last_j].name << endl;
            else cout << "UNDETERMINED" << endl;
        }
        if (t != 0) cout << endl;
    }

    return 0;
}