#include <bits/stdc++.h>
#define rep(i, n) for(int i = 0; i < n; ++i)
using namespace std;

typedef long long ll;

void testCase(int n, int m) {
    set<ll> jack, jill;
    ll val;
    rep(i, n) {
        cin >> val; jack.insert(val);
    }
    rep(i, m) {
        cin >> val; jill.insert(val);
    }

    ll sum = 0;
    for (auto val : jack) {
        if (jill.find(val) != jill.end()) {
            sum++;
        }
    }
    cout << sum << endl;
}

int main() {

    int n, m;
    while (true) {
        cin >> n >> m;
        if ((!n) && (!m)) break;
        testCase(n, m);
    }

    return 0;
}