#include <bits/stdc++.h>
using namespace std;

int main() {
    int n; cin >> n;
    while (n--) {
        int a, b, c; cin >> a >> b >> c;
        bool can = false;
        for (int x = -100; x <= 100; ++x) {
            for (int y = -100; y <= 100; ++y) {
                for (int z = -100; z <= 100; ++z) {
                    if (x==y || y==z || z==x) continue;
                    bool f1, f2, f3;
                    f1 = (x + y + z == a);
                    f2 = (x*y*z == b);
                    f3 = (x*x + y*y + z*z == c);
                    if (f1 && f2 && f3) {
                        can = true;
                        cout << x << " " << y << " " << z << endl;
                        break;
                    }
                }
                if (can) break;
            }
            if (can) break;
        }
        if (!can) cout << "No solution." << endl;
    }
    return 0;
}