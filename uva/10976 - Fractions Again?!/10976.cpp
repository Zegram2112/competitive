#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

struct Info {
    int x, y;

    Info(int x, int y): x(x), y(y) {};

    void print(int k) {
        if (x < y) {
            int temp = x;
            x = y;
            y = temp;
        }
        printf("1/%d = 1/%d + 1/%d\n", k, x, y);
    }
};

int main() {
    int k;
    while(cin >> k) {
        vector<Info> results;
        set<int> nums;
        for (int x = 1; x <= 2*k; ++x) {
            if (nums.find(x) == nums.end() && x-k != 0) {
                int y = (k * x) / (x - k);
                if (y <= 0) continue;
                if (x*y == k*y + k*x) {
                    results.emplace_back(x, y);
                    nums.insert(x);
                    nums.insert(y);
                }
            }
        }
        printf("%d\n", results.size());
        for (auto res : results) {
            res.print(k);
        }
    }
}