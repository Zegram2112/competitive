#define loop(i,n) for(int i = 0; i<n; ++i)

#include <bits/stdc++.h>
using namespace std;

struct RFP {
    string name;
    float d;
    int r;

    bool better(RFP other) {
        if (r > other.r) {
            return true;
        } else if (r == other.r) {
            return (d < other.d);
        }
        return false;
    }
};

bool testCase(int number) {
    int n, p;
    string junk;
    cin >> n >> p;
    getline(cin, junk);
    if ((n == 0 && p == 0)||!cin) return false;
    else if (number != 1) {
        cout << "\n";
    }
    loop(j,n) getline(cin, junk);

    RFP bestRfp;
    bool defined = false;
    int pos;
    loop(i, p) {
        RFP rfp;
        getline(cin, rfp.name) >> rfp.d >> rfp.r;
        getline(cin, junk);
        loop(j, rfp.r) {
            getline(cin, junk);
        }
        if (!defined || rfp.better(bestRfp)) {
            bestRfp = rfp;
            defined = true;
            pos = i;
        }
    }

    cout << "RFP #" << number << "\n" << bestRfp.name << endl;

    return true;
}

int main() {

    int num = 1;
    while(true) {
        bool res = testCase(num);
        if (!res)
            break;
        ++num;
    }

    return 0;
}