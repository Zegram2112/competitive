#define loop(n) for (int i = 0; i < n; ++i)
#include <bits/stdc++.h>
using namespace std;

class Array {
public:

    Array(int B, int CD, int D, vector<int> & L, vector<int> & U):
        B(B), D(D), L(L), U(U) {
            C.resize(D+1, 0);
            C[D] = CD;
            for (int i = D-1; i >=1; --i) {
                C[i] = C[i+1] * (U[i] - L[i] +1);
            }
            C[0] = B;
            for (int i = 1; i < D+1; ++i) {
                C[0] -= C[i] * L[i-1];
            }
        }

    Array() = default;

    int address(const vector<int> & refs) {
        int asum = 0;

        loop(D+1) {
            if (i == 0) {
                asum += C[0];
            } else {
                asum += C[i]*refs[i-1];
            }
        }

        return asum;
    }

    int B, D;
    vector<int> L, U, C;
};

int main() {

    map<string, Array> arrays;

    int n, r;
    string name;
    int B, CD, D, l, u;
    cin >> n >> r;
    vector<int> L, U;
    loop(n) {
        L.clear();
        U.clear();
        cin >> name >> B >> CD >> D;
        loop(D) {
            cin >> l >> u;
            L.push_back(l);
            U.push_back(u);
        }

        arrays[name] = Array(B, CD, D, L, U);
    } 
    getline(cin, name); // get to the next line

    string line;
    istringstream iss;
    int ref, address;
    vector<int> refs;
    loop(r) {
        refs.clear();
        getline(cin, line);
        iss.str(line);
        iss.clear();
        iss >> name;
        while (iss >> ref) {
            refs.push_back(ref);
        }
        address = arrays[name].address(refs); 
        cout << name << "[";
        for (int i = 0; i < refs.size(); ++i) {
            cout << refs[i];
            if (i == refs.size() - 1) {
                break;
            } else {
                cout << ", ";
            }
        }
        cout << "] = " << address << endl;;
    }

    return 0;
}