#include <bits/stdc++.h>

using namespace std;

void testCase(string line) {
    list<char> output;
    auto it = output.end();

    for (char c : line) {
        switch (c) {
            case '[':
                it = output.begin();
                break;
            case ']':
                it = output.end();
                break;
            default:
                it = output.insert(it, c);
                ++it;
                break;
        }
    }

    string sOutput(output.begin(), output.end());
    cout << sOutput << endl;
}

int main() {
    
    string testLine;
    while (getline(cin, testLine)) {
        testCase(testLine);
    }

    return 0;

}
