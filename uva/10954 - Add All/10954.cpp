#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

bool testCase() {
    int n; cin >> n;
    if (n == 0) return false;
    
    int val;
    priority_queue<ll, vector<ll>, greater<ll>> pq;
    for (int i = 0; i < n; ++i) {
        cin >> val;
        pq.push(val);
    }

    int minS = 0;
    while (true) {
        int s = pq.top(); pq.pop();
        s += pq.top(); pq.pop();
        pq.push(s);
        minS += s;
        if (pq.size() <= 1) break;
    }

    cout << minS << endl;

    return true;
}

int main() {

    while (true) {
        if (!testCase()) break;
    }

    return 0;
}
