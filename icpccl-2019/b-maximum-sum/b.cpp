#include <bits/stdc++.h>
using namespace std;


typedef long long ll;
typedef vector<int> vi;
typedef pair<int, int> pii;
typedef vector<pii> vpii;

const int minf = -1e8;


ostream & operator<<(ostream & os, const pii & v) {
    cout << "(" << v.first << "," << v.second << ")";
    return os;
}

struct SegmentTree {

    vi a;
    vpii st;
    int N;

    SegmentTree(vi & a): a(a) {
        N = a.size();
        st.resize(4*N+5);
        build(1, 0, N-1, a);
    }

    void build(int n, int l, int r, vi & a) {
        if (l == r) {
            st[n] = {a[l], minf};
            return;
        }
        build(2*n, l, (l+r)/2, a);
        build(2*n+1, (l+r)/2+1, r, a);

        int els[4] = {st[2*n].first, st[2*n+1].first,
            st[2*n].second, st[2*n+1].second};
        sort(begin(els), end(els));
        st[n] = {els[3], els[2]};
        //cout << n << ": " << st[n] << endl;
    }

    void update(int i, int v) {
        update(1, 0, N-1, i, v);
    }

    void update(int n, int l, int r, int i, int v) {
        // afuera
        if (r < i || i < l) {
            return;
        }
        // hoja
        if (l == r) {
            st[n] = {v, minf};
            return;
        }
        // derivo hijos
        update(2*n, l, (l+r)/2, i, v);
        update(2*n+1, (l+r)/2+1, r, i, v);

        int els[4] = {st[2*n].first, st[2*n+1].first,
            st[2*n].second, st[2*n+1].second};
        sort(begin(els), end(els));
        st[n] = {els[3], els[2]};
    }

    pii query(int i, int j) {
        return query(1, 0, N-1, i, j);
    }

    pii query(int n, int l, int r, int i, int j) {
        // afuera
        if (r < i || j < l) {
            return {minf, minf};
        }
        // adentro
        if (i <= l && r <= j) {
            return st[n];
        }
        // derivo hijos
        pii q1 = query(2*n, l, (l+r)/2, i, j);
        pii q2 = query(2*n+1, (l+r)/2+1, r, i, j);
        int els[4] = {q1.first, q1.second, q2.first, q2.second};
        sort(begin(els), end(els));
        return {els[3], els[2]};
    }

};

void print(vpii & a) {
    cout << a[0];
    for (int i = 1; i < a.size(); ++i) {
        cout << " " << a[i];
    }
    cout << endl;
}

int main() {
    
    int n, val, q, x, y;
    char action;
    vi a;
    cin >> n;
    a.resize(n+1);
    for (int i = 1; i <= n; ++i) {
        cin >> a[i]; 
    }
    SegmentTree st(a);
    cin >> q;
    //print(st.st);
    for (int i = 0; i < q; ++i) {
        cin >> action >> x >> y;
        if (action == 'Q') {
            pii res = st.query(x, y);
            cout << res.first + res.second << endl;
        } else {
            st.update(x, y);
            //print(st.st);
        }
    }

    return 0;

}
